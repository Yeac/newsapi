package com.example.newsapi.ui.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs

import com.example.newsapi.R
import com.example.newsapi.ui.tops.TopsFragmentArgs
import com.example.newsapi.util.ShowToastMessageLong
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_details.*
import org.koin.android.ext.android.inject

class DetailsFragment : Fragment() {

    private val detailsViewModel: DetailsViewModel by inject()
    val arguments by navArgs<DetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detailsViewModel.setArticle(arguments.articleObj!!)
        detailsViewModel.setPosition(arguments.articlePos)

        setupViews()
    }

    private fun setupViews() {
        tv_details_title.text = detailsViewModel.objArticle.title
        tv_details_author.text = detailsViewModel.objArticle.author
        tv_details_date.text = detailsViewModel.objArticle.publishedAt
        tv_details_content.text = detailsViewModel.objArticle.content
        tv_details_url.text = detailsViewModel.objArticle.url
        Picasso
            .get()
            .load(detailsViewModel.objArticle.urlToImage)
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(iv_details_img)
    }

}
