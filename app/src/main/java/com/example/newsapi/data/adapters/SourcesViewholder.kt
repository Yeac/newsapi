package com.example.newsapi.data.adapters

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapi.R

class SourcesViewholder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    val name = itemView.findViewById<TextView>(R.id.tv_item_nombre)
    val desc = itemView.findViewById<TextView>(R.id.tv_item_desc)
    val language = itemView.findViewById<TextView>(R.id.tv_item_language)
    val country = itemView.findViewById<TextView>(R.id.tv_item_country)

    fun onItemClick(onItemClick: () -> Unit) {
        itemView.setOnClickListener { onItemClick() }
    }

}