package com.example.newsapi.di

import com.example.newsapi.ui.details.DetailsFragment
import com.example.newsapi.ui.sources.SourcesFragment
import com.example.newsapi.ui.tops.TopsFragment
import org.koin.dsl.module

val moduleFragment = module {

    factory { SourcesFragment() }
    factory { TopsFragment() }
    factory { DetailsFragment() }

}