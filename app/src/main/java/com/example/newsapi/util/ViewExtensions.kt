package com.example.newsapi.util

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun View.ShowToastMessageLong(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun View.ShowToastMessageShort(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}

fun View.ShouldWeHideIt(boolean: Boolean) {
    this.visibility = if (boolean) View.GONE else View.VISIBLE
}

