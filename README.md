# newsapi

Contenido:
- Arquitectura MVVM
- Lifecycle components (viewmodel, livedata, room, navigation)
- Recyclerview components (adapter, viewholder)
- Materialdesign library
- Coroutines
- Retrofit
- Koin di api
- API: newsapi.org