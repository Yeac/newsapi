package com.example.newsapi.data.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapi.R
import com.example.newsapi.data.local.models.Articles
import com.squareup.picasso.Picasso

class ArticleAdapter(
    private val list: List<Articles>,
    private val listener: ArticleListener<Articles>
) : RecyclerView.Adapter<ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder =
        ArticleViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_tops, parent, false)
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val objArticle = list[position]
        holder.title.text = objArticle.title
        holder.desc.text = objArticle.description
        holder.date.text = objArticle.publishedAt
        Picasso
            .get()

            .load(objArticle.urlToImage)
            .resize(80, 80)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(holder.image)
        holder.onClickArticle { listener.OnArticleClick(objArticle, position) }
    }

}