package com.example.newsapi.data.remote

import com.example.newsapi.data.local.models.Articles
import com.example.newsapi.data.local.models.Sources

class BaseResponse(
    //get sources
    val status: String,
    val sources: List<Sources>,

    //get top articles y search
    val totalResults: Int,
    val articles: List<Articles>,

    //error
    val code: String,
    val message: String

)