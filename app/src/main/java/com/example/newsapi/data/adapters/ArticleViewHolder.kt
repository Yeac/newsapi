package com.example.newsapi.data.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapi.R

class ArticleViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    val title = itemView.findViewById<TextView>(R.id.tv_tops_title)
    val desc = itemView.findViewById<TextView>(R.id.tv_tops_desc)
    val date = itemView.findViewById<TextView>(R.id.tv_tops_date)
    val image = itemView.findViewById<ImageView>(R.id.iv_tops_img)

    fun onClickArticle(onItemClick: () -> Unit) {
        itemView.setOnClickListener { onItemClick() }
    }

}