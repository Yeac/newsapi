package com.example.newsapi.data.adapters

interface SourceListener<T> {

    fun onSourceClickListener(item: T, position: Int)

}