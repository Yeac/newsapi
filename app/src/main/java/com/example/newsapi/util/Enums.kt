package com.example.newsapi.util

enum class Status(value: String) {
    OK(value = "ok"),
    ERROR(value = "error")
}