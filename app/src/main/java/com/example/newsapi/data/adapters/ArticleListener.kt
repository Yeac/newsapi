package com.example.newsapi.data.adapters

interface ArticleListener<T> {

    fun OnArticleClick(item: T, position: Int)

}