package com.example.newsapi.di

val appModules = listOf(
    moduleFragment,
    moduleViewModel,
    moduleNetwork,
    moduleRepository
)