package com.example.newsapi.di

import com.example.newsapi.ui.details.DetailsViewModel
import com.example.newsapi.ui.sources.SourcesViewModel
import com.example.newsapi.ui.tops.TopsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val moduleViewModel = module {

    viewModel { SourcesViewModel(get()) }
    viewModel { TopsViewModel(get()) }
    viewModel { DetailsViewModel(get()) }

}