package com.example.newsapi.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.newsapi.data.local.dao.ArticlesDao
import com.example.newsapi.data.local.dao.SourcesDao
import com.example.newsapi.data.local.models.Sources
import com.example.newsapi.data.local.util.Converters

@Database(
    entities = [Sources::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDataBase : RoomDatabase() {

    abstract fun getArticlesDao(): ArticlesDao
    abstract fun getSourcesDao(): SourcesDao

    companion object {

        @Volatile
        private var instance: AppDataBase? = null

        private val LOCK = Any()

        operator fun invoke(context: Context) =
            instance ?: synchronized(LOCK) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDataBase::class.java,
                "MiDatabase.db"
            ).build()
    }
}