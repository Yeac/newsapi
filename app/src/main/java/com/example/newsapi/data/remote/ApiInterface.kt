package com.example.newsapi.data.remote

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("sources")
    suspend fun getAllSources(
        @Query("apiKey") apiKey: String
    ): Response<BaseResponse>

    @GET("top-headlines")
    suspend fun getTopHeadlines(
        @Query("apiKey") apiKey: String,
        @Query("sources") sourceId: String
    ): Response<BaseResponse>

    @GET("everything")
    suspend fun getSearchArticles(
        @Query("apiKey") apiKey: String,
        @Query("q") qSearch: String
    ): Response<BaseResponse>

}