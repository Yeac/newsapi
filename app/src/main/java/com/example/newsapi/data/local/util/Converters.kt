package com.example.newsapi.data.local.util

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    @TypeConverter
    fun listToJson(list: List<String>): String? {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun jsonToList(json: String): List<String>? {
        val objects = Gson().fromJson(json, Array<String>::class.java) as Array<String>
        return objects.toList()
    }

    @TypeConverter
    inline fun <reified T> objectToJson(obj: T): String? {
        return Gson().toJson(obj)
    }

    @TypeConverter
    inline fun <reified T> jsonToObject(json: String): T? {
        val gson = Gson()
        val jsonType = object : TypeToken<T>() {}.type
        return gson.fromJson(json, jsonType)
    }

}