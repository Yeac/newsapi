package com.example.newsapi.ui.details

import androidx.lifecycle.ViewModel
import com.example.newsapi.data.local.models.Articles
import com.example.newsapi.data.repository.DetailsRepository
import java.text.FieldPosition

class DetailsViewModel(
    private val detailRepository: DetailsRepository
) : ViewModel() {

    lateinit var objArticle: Articles
    lateinit var position: Any

    fun setArticle(item: Articles) {
        objArticle = item
    }

    fun setPosition(pos: Int) {
        position = pos
    }


}
