package com.example.newsapi.data.repository

import androidx.lifecycle.liveData
import com.example.newsapi.data.AppDataBase
import com.example.newsapi.data.remote.ApiClient
import com.example.newsapi.data.remote.SafeApiRequest
import com.example.newsapi.util.API_KEY
import com.example.newsapi.util.Status.OK
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class SourceRepository(
    private val api: ApiClient,
    private val db: AppDataBase
) : SafeApiRequest() {

    fun getSourcesListCoroutines() = CoroutineScope(Dispatchers.IO)
        .launch {
        }

    fun getSourcesListLiveData() = liveData {
        val response = apiRequest { api.retrofit.getAllSources(API_KEY) }
        val list = response.sources
        emit(list)
    }

}