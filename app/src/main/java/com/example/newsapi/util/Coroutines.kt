package com.example.newsapi.util

import kotlinx.coroutines.*

object Coroutines {

    fun main(workmain: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.Main)
            .launch {
                workmain()
            }

    fun io(workio: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.IO)
            .launch {
                workio()
            }

    fun <T : Any> ioThenMain(work: suspend (() -> T?), callback: ((T?) -> Unit)) =
        CoroutineScope(Dispatchers.Main)
            .launch {
                val data = CoroutineScope(Dispatchers.IO)
                    .async rt@{
                        return@rt work()
                    }.await()
                callback(data)
            }

    fun <T> lazyDeferred(block: suspend CoroutineScope.() -> T): Lazy<Deferred<T>> {
        return lazy {
            GlobalScope.async(start = CoroutineStart.LAZY) {
                block.invoke(this)
            }
        }
    }

    fun <T> returnLiveData(funcion: suspend CoroutineScope.() -> T): Lazy<Deferred<T>> {
        return lazy {
            CoroutineScope(Dispatchers.IO)
                .async {
                    funcion.invoke(this)
                }
        }
    }


}