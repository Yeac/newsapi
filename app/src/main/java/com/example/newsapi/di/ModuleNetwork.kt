package com.example.newsapi.di

import com.example.newsapi.data.remote.ApiClient
import com.example.newsapi.data.remote.NetworkInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val moduleNetwork = module {

    single { NetworkInterceptor(androidContext()) }
    single { ApiClient(get()) }

}