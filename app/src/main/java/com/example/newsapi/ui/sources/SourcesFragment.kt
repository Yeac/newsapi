package com.example.newsapi.ui.sources

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.newsapi.R
import com.example.newsapi.data.adapters.SourceListener
import com.example.newsapi.data.adapters.SourcesAdapter
import com.example.newsapi.data.local.models.Sources
import com.example.newsapi.ui.util.NavigationDirections.Companion.navigateToTops
import com.example.newsapi.util.ShouldWeHideIt
import kotlinx.android.synthetic.main.fragment_sources.*
import org.koin.android.viewmodel.ext.android.viewModel

class SourcesFragment(
) : Fragment(), SourceListener<Sources> {

    private val sourcesViewModel: SourcesViewModel by viewModel()
    private val navController: NavController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_sources, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sourcesViewModel
            .listSources
            .observe(viewLifecycleOwner, Observer {
                buildList(it)
            })
    }

    private fun buildList(list: List<Sources>) {
        changeFragmentView(!list.isNullOrEmpty())
        rv_list_source.adapter = null
        rv_list_source.adapter = SourcesAdapter(list, this)
        rv_list_source.layoutManager = LinearLayoutManager(context)
    }

    override fun onSourceClickListener(item: Sources, position: Int) {
        navController.navigate(navigateToTops(item.id, item.name))
    }

    private fun changeFragmentView(status: Boolean) {
        rv_list_source.ShouldWeHideIt(!status)
        pb_list_source.ShouldWeHideIt(status)
    }

}
