package com.example.newsapi.data.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapi.R
import com.example.newsapi.data.local.models.Sources
import com.example.newsapi.util.getCountryName
import com.example.newsapi.util.getLanguageName

class SourcesAdapter(
    private val list: List<Sources>,
    private val listener: SourceListener<Sources>
) : RecyclerView.Adapter<SourcesViewholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SourcesViewholder =
        SourcesViewholder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_source, parent, false)
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SourcesViewholder, position: Int) {
        val objSource = list[position]

        holder.name.text = objSource.name
        holder.desc.text = objSource.description
        holder.country.text = getCountryName(objSource.country)
        holder.language.text = getLanguageName(objSource.language)
        holder.onItemClick { listener.onSourceClickListener(objSource, position) }
    }


}