package com.example.newsapi.di

import com.example.newsapi.data.AppDataBase
import com.example.newsapi.data.preferences.PreferenceProdiver
import com.example.newsapi.data.repository.DetailsRepository
import com.example.newsapi.data.repository.SourceRepository
import com.example.newsapi.data.repository.TopsRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val moduleRepository = module {

    single { AppDataBase(androidContext()) }
    single { SourceRepository(get(), get()) }
    single { TopsRepository(get(), get()) }
    single { DetailsRepository(get(), get()) }
    single { PreferenceProdiver(androidContext()) }

}

