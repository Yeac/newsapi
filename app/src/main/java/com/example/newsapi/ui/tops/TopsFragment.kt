package com.example.newsapi.ui.tops

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.newsapi.R
import com.example.newsapi.data.adapters.ArticleAdapter
import com.example.newsapi.data.adapters.ArticleListener
import com.example.newsapi.data.local.models.Articles
import com.example.newsapi.ui.util.NavigationDirections.Companion.navigateToDetails
import com.example.newsapi.util.ShouldWeHideIt
import com.example.newsapi.util.ShowToastMessageLong
import kotlinx.android.synthetic.main.fragment_tops.*
import org.koin.android.viewmodel.ext.android.viewModel

class TopsFragment : Fragment(), ArticleListener<Articles> {

    val topsViewModel: TopsViewModel by viewModel()
    val arguments by navArgs<TopsFragmentArgs>()
    val navController: NavController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_tops, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        topsViewModel.setTopSourceValue(arguments.idSource)
        topsViewModel
            .topsList
            .observe(viewLifecycleOwner, Observer {
                buildTopList(it)
            })
    }

    private fun buildTopList(list: List<Articles>) {
        changeFragmentView(!list.isNullOrEmpty())
        rv_list_top.adapter = null
        rv_list_top.adapter = ArticleAdapter(list, this)
        rv_list_top.layoutManager = LinearLayoutManager(context)
        //revisar
//        rv_list_top.layoutManager.scrollToPosition()
//        rv_list_top.layoutManager.findViewByPosition()
    }

    override fun OnArticleClick(item: Articles, position: Int) {
        navController.navigate(navigateToDetails(item.title, position, item))
    }

    private fun changeFragmentView(status: Boolean) {
        rv_list_top.ShouldWeHideIt(!status)
        pb_list_top.ShouldWeHideIt(status)
    }

}
