package com.example.newsapi.util

import java.io.IOException

class NoInternetException(message: String) : IOException(message)

class ApiException(message: String) : IOException(message)

class JsonSyntaxException(message: String) : IOException(message)

class ApiTimeOutException(message: String) : IOException(message)