package com.example.newsapi.ui.util

import androidx.navigation.NavDirections
import com.example.newsapi.data.local.models.Articles
import com.example.newsapi.ui.sources.SourcesFragmentDirections.Companion.actionListSourceToSourceTops
import com.example.newsapi.ui.tops.TopsFragmentDirections.Companion.actionSourceTopsToArticleDetails

class NavigationDirections {

    companion object {
        fun navigateToTops(id: String, name: String): NavDirections =
            actionListSourceToSourceTops(id, name)

        fun navigateToDetails(id: String, position: Int, article: Articles): NavDirections =
            actionSourceTopsToArticleDetails(id, position, article)
    }

}