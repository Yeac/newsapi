package com.example.newsapi.ui.sources

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.newsapi.data.local.models.Sources
import com.example.newsapi.data.repository.SourceRepository

class SourcesViewModel(
    private val repository: SourceRepository
) : ViewModel() {

    val listSources: LiveData<List<Sources>> by lazy {
        repository.getSourcesListLiveData()
    }

}
