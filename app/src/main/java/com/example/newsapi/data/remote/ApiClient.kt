package com.example.newsapi.data.remote

import com.example.newsapi.util.BASE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient(
    interceptor: NetworkInterceptor
) {

    val retrofit: ApiInterface by lazy {
        val client = OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .build()

        Retrofit
            .Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiInterface::class.java)
    }
}