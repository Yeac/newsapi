package com.example.newsapi.data.repository

import androidx.lifecycle.liveData
import com.example.newsapi.data.AppDataBase
import com.example.newsapi.data.remote.ApiClient
import com.example.newsapi.data.remote.SafeApiRequest
import com.example.newsapi.util.API_KEY
import com.example.newsapi.util.ApiException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.TimeoutException

class TopsRepository(
    private val api: ApiClient,
    private val db: AppDataBase
) : SafeApiRequest() {

    fun getTopArticlesFromSource(source: String) = liveData {
        val response = apiRequest { api.retrofit.getTopHeadlines(API_KEY, source) }
        emit(response.articles)
    }

    fun getTopArticlesFrom(source: String) =
        CoroutineScope(Dispatchers.IO)
            .launch {
                try {
                    api.retrofit.getTopHeadlines(API_KEY, source)
                    return@launch
                } catch (e: IOException) {
                    println("Error: IOException")
                } catch (e: ApiException) {
                    println("Error: ApiException")
                } catch (e: TimeoutException) {
                    println("Error: Timeout")
                }
            }
}