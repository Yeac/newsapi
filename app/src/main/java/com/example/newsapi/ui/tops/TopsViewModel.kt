package com.example.newsapi.ui.tops

import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.navArgs
import com.example.newsapi.data.repository.TopsRepository

class TopsViewModel(
    private val repository: TopsRepository
) : ViewModel() {

    lateinit var topSource: String

    val topsList by lazy {
        repository.getTopArticlesFromSource(topSource)
    }

    fun setTopSourceValue(value: String) {
        topSource = value
    }


}
